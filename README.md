# Material de apoyo para Diseño Micro electrónico

## Instalación de las herramientas en Debian:

#### Instalar Python3
sudo apt-get install -y python3

#### Instalar pip3
sudo apt-get install -y python3-pip

##### Ejecutar un programa
python3 nombre_programa.py 

#### Instalar el editor nano
sudo apt-get install -y nano

#### Instalar multiplexor de terminal
sudo apt-get install -y tmux

#### Documentación de Python
https://docs.python.org/3/

#### Documentación de MicroPython
- MicroPython: https://docs.micropython.org/en/latest/
- MicroPython ESP32: https://docs.micropython.org/en/latest/esp32/quickref.html

#### Documentación de BASH Shell
- Bash Guide for Beginner: https://tldp.org/LDP/Bash-Beginners-Guide/html/Bash-Beginners-Guide.html
- Advanced Bash-Scripting Guide: https://tldp.org/LDP/abs/html/abs-guide.html

#### Guía rápida de nano y tmux
- Blog: https://diegorestrepoleal.blogspot.com/2022/02/guia-rapida-de-nano-y-tmux.html

#### Configurar LAMP Amazon Linux 2 para desplegar aplicación web con Python (Flask y WSGI)
- Blog: https://diegorestrepoleal.blogspot.com/2022/03/configurar-lamp-amazon-linux-2-para.html
