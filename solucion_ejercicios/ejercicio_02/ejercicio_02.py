def fahrenheit2celsius(temperatura):
    return (5/9)*(temperatura - 32)


def celsius2fahrenheit(temperatura):
    return (9/5)*temperatura + 32


def run():
    unidad = str(input('''Ingrese la unidad de temperatura:
                        (F) -> °F a °C:
                        (C) -> °C a °F:
                      ---->  '''))
    temperatura = float(input('Ingrese el valor de temperatura: '))

    if unidad == 'F':
        resultado = fahrenheit2celsius(temperatura)
        print(f'{temperatura}°F = {resultado}°C')
    elif unidad == 'C':
        resultado = celsius2fahrenheit(temperatura)
        print(f'{temperatura}°C = {resultado}°F')
    else:
        print('La elección no es válida.')


if __name__ == '__main__':
    run()
