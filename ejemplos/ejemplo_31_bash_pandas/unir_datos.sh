#!/bin/bash


archivos=`ls *.xlsx`


if [ ! -d resultados_$1 ];
then
    mkdir resultados_$1
fi


echo $archivos >> archivos.txt


cat > unir.py <<EOF
import pandas as pd
import matplotlib.pyplot as plt


def run():
    lista_file = []

    with open('archivos.txt', 'r') as f:
        for line in f:
            for word in line.split():
                lista_file.append(word)

    lista_df = []

    for i in range(len(lista_file)):
        data = pd.read_excel(lista_file[i])
        lista_df.append(data)

    result = pd.concat(lista_df)
    result['registro'] = pd.to_datetime(result['fecha'] + ' ' + result['hora'])
    result['fecha'] = result['registro']
    result.drop(['registro', 'hora', 'numCasa'], axis=1, inplace=True)
    result = result.sort_values(by="fecha")

    print('Temperatura')
    df_T = result[['temperatura1', 'temperatura2', 'temperatura3', 'temperatura4']]
    boxplot = plt.figure()
    df_T.boxplot()
    boxplot.show()
    boxplot.savefig(r'resultados_$1/boxplot_Temperatura_$1.svg', dpi=800)

    result.to_excel(r'resultados_$1/union_$1.xlsx', index = False)

    result = result.describe().transpose()
    result.to_excel(r'resultados_$1/estadistica_$1.xlsx')


if __name__ == '__main__':
    run()

EOF


echo
echo "Ejecutar unir.py"
echo
python3 unir.py


echo
echo "Eliminar archivos"
echo
rm -rf archivos.txt unir.py

exit 0
